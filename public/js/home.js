var app = {
    wallet_name: "",
    status: false,
    info: [],
    error: ""
};

jQuery(document).ready(function($) {
    var app_info = $.get("data/info.php"); 
    
    app_info.done(function(data) {
        app.name = data.name;
        app.status = true;
        app.info = data;
        home_init();
        show_transaction();
    })
    .fail(function(xhr) {
        var output = $.parseJSON(xhr.responseText);
        alert(output.message);
        app.error = output.message;
        $("#link_walletinfo > img").attr("src", "assets/info_red_ico.png");
    });

    function home_init() {
        $("#app_wallet_name").append(app.info.name);
        $("#home_balance").text(app.info.balance);
        $("#home_info").append('<p class="lead">unconfirmed: ' + app.info.balance_unconfirmed + ' SMART, immature: ' + app.info.balance_immature + ' SMART</p>');
        $("#link_send").on('click', function(e){
            e.preventDefault();
            $("#modal_send").modal("show");
        });
        $("#link_receive").on('click', function(e){
            e.preventDefault();
            $("#modal_receive").modal("show");
        });
    }

    $("#link_walletinfo").on('click', function(e){
        e.preventDefault();
        $("#modal_info").modal("show");
    });

    $(".btn-refresh").on("click", function(e){
        e.preventDefault();
        location.reload();
    });

    function show_transaction() {
        $('#table_transactions').footable({
            sorting: {
                enabled: true
            },
            columns: [                
                { "name": "time", "title": "Time", "type":"date","formatString": "MMM DD, YYYY HH:mm:ss", "style": {"minWidth": 185}, "sorted": true, "direction": "DESC" },
                { "name": "category", "title": "Category", "style": {"minWidth": 125}},
                { "name": "address", "title": "Address" },
                { "name": "amount", "title": "Amount", "type": "number" }
            ],
            rows: $.get("data/transactions.php"),
        });
    }

    /* event of the info modal */
    $("#modal_info").on('show.bs.modal', function () {
        if (app.status) {
            $("#modalvalue_rpcversion").text(app.info.rpc_version);
            $("#modalvalue_walletversion").text(app.info.wallet_version);
            $("#modalvalue_blockcount").text(app.info.block_count);
            $("#modalvalue_connectioncount").text(app.info.connection_count);
        } else {
            $("#modal_info_body").html('<div class="alert alert-danger" role="alert">' + app.error + '</div>');
        }
    });

    /* event of the receive modal */
    $("#modal_receive").on('show.bs.modal', function () {
        $.get("data/address.php", function(data) {
            $("#input_receive_address").val(data.address);
            $("#input_receive_amount").val("");
            $("#img_receive_address").attr("src", "data/qr.php?address=" + data.address);
            $("#input_receive_amount").on("change keyup paste click", function() {
                if ($(this).val() !== "") {
                    $("#img_receive_address").attr("src", "data/qr.php?address=" + data.address + "?amount=" + $(this).val());
                } else {
                    $("#img_receive_address").attr("src", "data/qr.php?address=" + data.address);
                }
            })
        });
    });

    /* event of the send modal */
    $("#modal_send").on('show.bs.modal', function () {
        $("#input_send_address").val("");
        $("#input_send_amount").val("");
        $("#modal_send_notification").html("");
    });

    $("#btn_send").on('click', function (e) {
        e.preventDefault();
        /* validation here */
        // insufficient funds
        var send_amount = parseFloat($("#input_send_amount").val());
        console.log(send_amount);
        if ($("#input_send_address").val() !== "" && send_amount != 0 && !isNaN(send_amount)) {
            var app_send = $.post("data/send.php", {
                address: $("#input_send_address").val(),
                amount: $("#input_send_amount").val()
            }, 'json');
            app_send.done(function(output) {
                location.reload();
            })
            .fail(function(xhr) {
                var output = $.parseJSON(xhr.responseText);
                $("#modal_send_notification").html('<div class="alert alert-danger" role="alert">' + output.message + '</div>');
            });
        } else {
            $("#modal_send_notification").html('<div class="alert alert-danger" role="alert">Please insert the valid SmartCash address and amount</div>');
        }
    });
});
