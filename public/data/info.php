<?php
/*
 * SmartCash Web Wallet
 * 
 * A Custom Application of SmartCash Wallet.
 * 
 * Distributed under The MIT License
 * Copyright 2018 Hanan (saung.hanan [at] gmail [dot] com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining 
 * a copy of this software and associated documentation files (the "Software"), 
 * to deal in the Software without restriction, including without limitation 
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom 
 * the Software is furnished to do so, subject to the following conditions: 
 * 
 * The above copyright notice and this permission notice shall be included 
 * in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR 
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR 
 * OTHER DEALINGS IN THE SOFTWARE.
 * 
 */

include "../../init.php";

$http_code = 200;
try {
    $info = (object) $smartcash->getinfo();
    $walletinfo = (object) $smartcash->getwalletinfo();
    $data = [
        'name' => $wallet_name,
        'rpc_version' => $info->version,
        'wallet_version' => $walletinfo->walletversion,
        'balance' =>  round($walletinfo->balance, 4),
        'balance_unconfirmed' => round($walletinfo->unconfirmed_balance, 4),
        'balance_immature' => round($walletinfo->immature_balance, 4),
        'block_count' => $info->blocks,
        'connection_count' => $info->connections
    ];
} catch (Exception $e) {
    $http_code = 400;
    $data = ['message' => $e->getMessage()];
}

if (isset($http_code))
    http_response_code($http_code);
    
header('Content-type: application/json');
echo json_encode($data, JSON_PRETTY_PRINT);