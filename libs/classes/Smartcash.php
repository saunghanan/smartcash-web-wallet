<?php
/*
 * SmartCash Web Wallet
 * 
 * A Custom Application of SmartCash Wallet.
 * 
 * Distributed under The MIT License
 * Copyright 2018 Hanan (saung.hanan [at] gmail [dot] com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining 
 * a copy of this software and associated documentation files (the "Software"), 
 * to deal in the Software without restriction, including without limitation 
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom 
 * the Software is furnished to do so, subject to the following conditions: 
 * 
 * The above copyright notice and this permission notice shall be included 
 * in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR 
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR 
 * OTHER DEALINGS IN THE SOFTWARE.
 * 
 */


class Smartcash {
    private $username;
    private $password;
    private $url;
    private $id;
	
    /**
     * 
     * @param string $url
     * @param string $username
     * @param string $password
     */
    public function __construct($url, $username, $password) {
        // connection details
        $this->url = $url;
        $this->username = $username;
        $this->password = $password;

        // the request id
        $this->id = 1;
    }
	
    /**
     * 
     * @param string $method
     * @param array $params
     * @return array
     */
    public function __call($method, $params) {
        $params = array_values($params);
        $request = json_encode([
            'method' => strtolower($method),
            'params' => $params,
            'id' => $this->id
        ]);
						
        // performs the HTTP POST using curl
        $curl = curl_init();     
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);  
        curl_setopt($curl, CURLOPT_HTTPHEADER, ["Content-type: application/json"]);
        curl_setopt($curl, CURLOPT_URL, $this->url);
        curl_setopt($curl, CURLOPT_USERPWD, "{$this->username}:{$this->password}");  
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $request);
        $response = curl_exec($curl);  
        curl_close($curl);
		
        // process response
        if (!$response) throw new Exception("Unable to connect to {$this->url}", 10);
        
        $response = json_decode($response,true);
        
        if ($response['id'] != $this->id) throw new Exception("Incorrect response id (request id: {$this->id}, response id: {$response['id']})", 11);
        if (!is_null($response['error'])) throw new Exception("Request error: {$response['error']['message']}", 12);

        $this->id ++;
        return $response['result'];
	}
}
