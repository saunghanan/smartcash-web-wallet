# SmartCash Web Wallet

A Custom Application of SmartCash Wallet.

## What's SmartCash?

SmartCash is a self-funded, community-driven cryptocurrency focused on merchant adoption and ease of use.

For more information, see https://smartcash.cc  

## The feature of this app:

This initial release has feature:

- Web based and mobile first
- Balance and wallet information
- Send and receive SMART
- The latest transactions

## Download & Instalation

See INSTALL.md

## License 

This app is released under the terms of the MIT license. See LICENSE for more information or see http://opensource.org/licenses/MIT.