# Installation Instruction

## Requirements

- PHP 7+ with JSON & GD extensions.
- PHP Composer when you install from the source. Composer can be downloaded from: https://getcomposer.org/.
- The SmartCash's node client: https://smartcash.cc/wallets/#nodeclient

## Installation & Run 

Download the latest source from the repository https://bitbucket.org/saunghanan/smartcash-web-wallet/downloads/, extract and run `composer install`. 

Another way you can download from the released package without running the PHP Composer: https://bitbucket.org/saunghanan/smartcash-web-wallet/downloads/smartcash-web-wallet.zip

Run smartcashd or smartcash-qt with the `-server`argument, the app will communicate to node via HTTP JSON-RPC. Open the smartcash.conf file and add the following lines:

```
server=1
daemon=1
rpcuser=smartcash
rpcpassword=password
rpcallowip=127.0.0.1
rpcport=9679
```

Open the `config.php` file, and change the configuration:

```
// HTTP RPC-JSON Information
$rpc_url = "http://127.0.0.1:9679";
$rpc_username = "smartcash";
$rpc_password = "password";

// App Configuration
$wallet_name = "My Wallet";
$tx_latest_limit = 50;
```

Run the PHP Built-in Webserver

```
php -S 127.0.0.1:8080 -t public/
```

If you want the app accessing through your local network, you can change the argument `-S 0.0.0.0:8080` or install within the web server applications such as Apache or NGINX.
